<?php
namespace App\Controller;

use App\Repository\PropertyRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class HomeController extends AbstractController
{
/*
  private $twig;

  public function __construct(Environment $twig){
    $this->twig = $twig;
  }

  public function index(): Response
  {
    return new Response($this->twig->render('pages/home.html.twig'));
  }
*/

  public function index(PropertyRepository $repository): Response
  {
    $properties = $repository->findLatest();
    return $this->render('pages/home.html.twig', [
        'properties' => $properties
    ]);
  }


}


 ?>
