<?php

namespace App\Controller;

use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Repository\PropertyRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

class PropertyController extends AbstractController
{
  /*
   * @var PropertyRepository
   */
  private $repository;

  /*
   * @var EntityManagerInterface
   */
  private $em;

  public function __construct(PropertyRepository $repository, EntityManagerInterface $em)
  {
    $this->repository = $repository;
    $this->em = $em;
  }

  public function index(PaginatorInterface $paginator, Request $request): Response
  {
    /* Créer un bien manuellement
    $property = new Property();
    $property->setTitle('Mon premier bien')
      ->setPrice(200000)
      ->setRooms(4)
      ->setBedrooms(3)
      ->setDescription('Une petite descprition')
      ->setSurface(60)
      ->setFloor(4)
      ->setHeat(1)
      ->setCity('Montpellier')
      ->setAddress('15 Boulevard Gambetta')
      ->setPostalCode('34000');
    $em = $this->getDoctrine()->getManager();
    $em->persist($property);
    $em->flush();
    */
    /*
    $repository = $this->getDoctrine()->getRepository(Property::class);
    dump($repository);
    */
    /*
    $property = $this->repository->findAllVisible();
    //$property[0]->setSold(true);
    dump($property);
    //$this->em->flush();
    */

    $search = new PropertySearch();
    $form = $this->createForm(PropertySearchType::class, $search);
    $form->handleRequest($request);

    $properties = $paginator->paginate(
        $this->repository->findAllVisibleQuery($search),
        $request->query->getInt('page',1),
        12
    );
    return $this->render('property/index.html.twig', [
      'current_menu' => 'properties',
      'properties' => $properties,
      'form' => $form->createView()
    ]);
  }

/*
  public function show($slug, $id) : Response
  {
    $property = $this->repository->find($id);
    return $this->render('property/show.html.twig', [
        'property' => $property,
        'current_menu' => 'properties'
    ]);
  }
*/
  /*
   * @param Property $property
   * @return Response
   */

  public function show(Property $property, string $slug) : Response
  {
    if($property->getSlug() !== $slug){
      return $this->redirectToRoute('property.show', [
          'id' => $property->getId(),
          'slug' => $property->getSlug()
      ], 301);
    }
    return $this->render('property/show.html.twig', [
        'property' => $property,
        'current_menu' => 'properties'
    ]);
  }

}


?>
